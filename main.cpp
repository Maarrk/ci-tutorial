#include <iostream>

#include "sample.hpp"

int main() {
    uint32_t n = 4;
    std::cout << "n: " << n << " f(n): " << fibonacci(n) << std::endl;

    return 0;
}