//
// Created by marek on 20.02.19.
//

#define BOOST_TEST_MODULE sample test
#include <boost/test/unit_test.hpp>

#include "sample.hpp"

BOOST_AUTO_TEST_CASE( fibonacci0 )
{
    BOOST_CHECK( fibonacci(0) == 0 );
}

BOOST_AUTO_TEST_CASE( fibonacci1 )
{
    BOOST_CHECK( fibonacci(1) == 1 );
}

BOOST_AUTO_TEST_CASE( fibonacci2 )
{
    BOOST_CHECK( fibonacci(2) == 1 );
}

BOOST_AUTO_TEST_CASE( fibonacci5 )
{
    BOOST_CHECK( fibonacci(5) == 5 );
}