//
// Created by marek on 20.02.19.
//

int fibonacci(int n) {

    if(n <= 0) return 0;
    if(n == 1) return 1;

    int old = 0;
    int current = 1;
    int tmp;
    for(int i = 2; i<=n; i++){
        tmp = current;
        current += old;
        old = tmp;
    }

    return current;
}
